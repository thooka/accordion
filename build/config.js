import path from 'path';

const BASE_PATH = path.join(__dirname, '../');
    
export default {

    /*
        后台代理目标地址
     */
    API_URL: 'http://jonedev.ctg.com:3000/',
    PROXY_PREFIX: 'local-proxy',

    /*
        前端开发环境端口
     */
    SERVER_PORT: 8180,

    /**
     * 路径地址
     */
    PATH_SRC: path.join(BASE_PATH, 'src'),
    PATH_DIST: path.join(BASE_PATH, 'dist'),
    PATH_NODE_MODULES: path.join(BASE_PATH, 'node_modules'),

    /**
     * Webpack 配置
     */
    // 放外部引入库
    WEBPACK_FRAMEWORK: ['jquery', 'angular', 'angular-animate', 'angular-sanitize',
     'bootstrap_css', 'fontawesome_css', 'thooka_common_css', 'babel-polyfill'],
    WEBPACK_DEMO_ENTRY: [ path.join(BASE_PATH, 'src/demo/app.js') ],
    WEBPACK_DIST_ENTRY: [ path.join(BASE_PATH, 'src/scripts/index.js') ],
    WEBPACK_ENTRY_ORDER: { 'vendor': 0, framework: 1, app: 2 },

    BUILD_LIBRARY_NAME: 'accordion',

    /**
     * 浏览器兼容列表
     */
    CSS_PREFIX_LIST: ['> 1%', 'ie > 6', 'Firefox > 20']
}
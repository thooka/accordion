import webpack from 'webpack';
import config from './config';
import webpackDefaultConfig from './webpack.config';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';

webpackDefaultConfig.output = {
    path: config.PATH_DIST,
    filename: config.BUILD_LIBRARY_NAME + '.min.js',
    library: config.BUILD_LIBRARY_NAME,
    libraryTarget: 'umd',
    umdNamedDefine: true,
    publicPath: '',
};

// 与dev相比，禁用了source map, 所有图片均使用file-loader
webpackDefaultConfig.module.rules = webpackDefaultConfig.module.rules.concat([
    {
        test: /\.scss$/,
            use: [{
                loader: 'style-loader'
            }, {
                loader: 'css-loader',
                options: {
                    modules: true,
                    localIdentName: '[local]--[hash:base64:5]'
                }
            }, {
                loader: 'autoprefixer-loader',
                options: {
                    browsers: config.CSS_PREFIX_LIST
                }
            }, { 
                loader: 'sass-loader',
                options: {
                    includePaths: [ config.PATH_SRC + '/assets/styles' ]
                }
            }],
    }, {
        test: /\.css$/,
            use: [{
                loader: 'style-loader'
            }, {
                loader: 'css-loader'
            }, {
                loader: 'autoprefixer-loader',
                options: {
                    browsers: config.CSS_PREFIX_LIST
                }
            }],
    }, {
        test: /\.(jpe?g|png|gif)$/i,
        use: 'file-loader?name=images/[name]_[hash:base64:5].[ext]'
    }
]);

webpackDefaultConfig.plugins = webpackDefaultConfig.plugins.concat([

    /**
     * 压缩js
     */
    new webpack.optimize.UglifyJsPlugin({
        mangle: {
            except: ['$super', '$', 'exports', 'require', 'angular']
        }
    }),

    new webpack.DefinePlugin({ 'env':'"prod"' }),
]);

webpackDefaultConfig.externals = {
    'angular': {
        commonjs: "angular",
        amd: "angular",
        root: 'angular'
    },
    'jquery': {
        commonjs: 'jquery',
        commonjs2: 'jquery',
        amd: 'jquery',
        root: '$'
    },
    'angular-animate': {
        commonjs: 'angular-animate',
        amd: 'angular-animate'
    }, 
    'angular-sanitize': {
        commonjs: 'angular-sanitize',
        amd: 'angular-sanitize'
    }, 
    'bootstrap_css': {
        commonjs: 'bootstrap_css',
    }, 
    'fontawesome_css': {
        commonjs: 'fontawesome_css',
    }, 
    'thooka_common_css': {
        commonjs: 'thooka_common_css',
    }
};

export default webpackDefaultConfig;
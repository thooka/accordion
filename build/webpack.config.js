import webpack from 'webpack';
import config from './config';

export default {
    entry: {
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: [/node_modules/],
            use: [{
                // 使用ngInject的注释时，自动加载依赖
                loader: 'ng-annotate-loader'
            },{
                // es6语法支持
                loader: 'babel-loader'
            }]
        },{
            test: /\.html$/,
            exclude: /index.html/,
            use: [{
                // 打包html代码，相对于目录参数relativeTo
                loader: 'ngtemplate-loader?relativeTo=' + config.PATH_SRC + '/'
            }, {
                // html模块加载，不对attr进行修改
                loader: 'html-loader?attrs=false'
            }]
        },{
            //加载字体文件
            test: /\.(woff|woff2|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
            use: 'file-loader?name=fonts/[name].[ext]&prefix=fonts'
        }, {
            test: require.resolve("jquery"),
            loader: 'expose-loader?jQuery'
        }]
    },
    resolve: {
        alias: {
            // webpack配置时的一些别名
            bootstrap_css: config.PATH_NODE_MODULES + '/bootstrap/dist/css/bootstrap.css',
            fontawesome_css: config.PATH_NODE_MODULES + '/font-awesome/css/font-awesome.css',
            thooka_common_css: config.PATH_NODE_MODULES + '/@thooka/common-css/dist/styles/app.min.css'
        }
    },
    devtool: "eval",
    plugins: []
}
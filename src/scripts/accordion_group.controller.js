class Controller {
    constructor($scope) {
        "ngInject";

        this.$scope = $scope;

        this.group = [];
    }

    addItem(scope) {
        this.group.push(scope);

        scope.$on('$destroy', () => {
            this.removeItem(scope);
        });
    }

    removeItem(scope) {
        let index = this.group.indexOf(scope);
        if (index !== -1) {
            this.group.splice(index, 1);
        }
    };

    closeOthers(scope) {
        if(!this.$scope.closeOthers) {
            return;
        }

        this.group.forEach((_scope) => {
            if(scope !== _scope) {
                _scope.vm.expanded = false;
            }
        });
    }
}

export default Controller;
import accordion       from './accordion.directive';
import accordionGroup  from './accordion_group.directive';
import accordionHeading  from './accordion_heading.directive';
import accordionGroupAnimation from './accordion_group.animation';
import style from './accordion.scss';


export default
angular.module('pasp.ui.accordion', ['ngSanitize', 'ngAnimate'])
       .directive('uiAccordion', accordion.factory)
       .directive('uiAccordionGroup', accordionGroup.factory)
       .directive('uiAccordionHeading', accordionHeading.factory)
       .animation('.accordion-body-wrap', accordionGroupAnimation)
       .name;
export default () => {
    return {
        addClass: (element, className, done) => {
            let height = element.height();
            element.css({ height: height }).animate({
                height: 0
            }, 200, () => {
                element.removeAttr('style');
                done();
            });
        },
        removeClass: (element, className, done) => {
            let height = element.height();
            element.css({ height: 0 }).animate({
                height: height
            }, 200, () => {
                element.css({ height: 'auto' });
                done();
            });
        }
    }
}
class Controller {
    constructor($scope, $sce) {
        "ngInject";

        this.$scope = $scope;
        this.$sce = $sce;

        this.expanded = $scope.isOpen === true ? true : false;

        this.setHeading();
    }

    toggle() {
        this.expanded = !this.expanded;
        if(this.expanded) {
            this.$scope.uiAccordionGroup.closeOthers(this.$scope);
        }
    }

    setHeading(content) {
        this.heading = content ? this.$sce.trustAsHtml(content) : this.$scope.heading;
    }

}

export default Controller;
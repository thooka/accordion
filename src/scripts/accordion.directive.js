import templateUrl from './accordion.view.html';
import controller  from './accordion.controller';

class Directive {
    constructor() {
        return {
            scope: {
                heading: '@',
                isOpen: '=?'
            },
            require: ['^^uiAccordionGroup', 'uiAccordion'],
            restrict: 'E',
            replace: true,
            transclude: true,
            controller: controller,
            controllerAs: 'vm',
            templateUrl: (element, attrs) => {
                return attrs.templateUrl || templateUrl;
            },
            link: this.link
        };
    }

    link(scope, element, attr, ctrl) {
        const uiAccordionGroup = scope.uiAccordionGroup = ctrl[0];

        scope.icon = angular.isDefined(uiAccordionGroup.$scope.icon) ? uiAccordionGroup.$scope.icon : true;
        scope.iconPosition = uiAccordionGroup.$scope.iconPosition;

        uiAccordionGroup.addItem(scope);
    }

    static factory() {
        return new Directive();
    }
}

export default Directive;
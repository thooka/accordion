class Directive {
    constructor() {
        return {
            require: '^^uiAccordion',
            restrict: 'E',
            transclude: true,
            replace: true,
            template: '',
            link: this.link
        };
    }

    link(scope, element, attr, uiAccordion, transclude) {
        let content = transclude(scope, angular.noop);
        let html = angular.element('<span></span>').html(content);
        uiAccordion.setHeading(html.html());
    }

    static factory() {
        return new Directive();
    }
}

export default Directive;
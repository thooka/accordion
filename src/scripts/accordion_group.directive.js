import templateUrl from './accordion_group.view.html';
import controller  from './accordion_group.controller';

class Directive {
    constructor() {
        return {
            scope: {
                closeOthers: '=?',
                icon: '=?',
                iconPosition: '@'
            },
            restrict: 'E',
            replace: true,
            transclude: true,
            controller: controller,
            controllerAs: 'vm',
            templateUrl: (element, attrs) => {
                return attrs.templateUrl || templateUrl;
            },
            link: this.link
        };
    }

    link(scope, element, attr, ctrl) {
        scope.closeOthers = angular.isDefined(scope.closeOthers) ? scope.closeOthers : true;
    }

    static factory() {
        return new Directive();
    }
}

export default Directive;